<?php

namespace Drupal\Tests\media_remote_document\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\media\Entity\Media;
use Drupal\media_remote_document_test\ResourceMiddleware;

/**
 * Tests creating media items from remote document sources.
 *
 * @group media_remote_document
 */
class RemoteDocumentMediaTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'file',
    'image',
    'media',
    'media_remote_document',
    'media_remote_document_test',
    'system',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set up the database so that we can create remote document media items.
    $this->installConfig('media');
    $this->installConfig('media_remote_document');
    $this->installEntitySchema('file');
    $this->installEntitySchema('media');
    $this->installEntitySchema('user');
    $this->installSchema('file', ['file_usage']);
  }

  /**
   * Data provider for ::testRemoteDocumentMedia().
   *
   * @return array[]
   *   The test cases.
   */
  public static function providerRemoteDocumentMedia(): array {
    return [
      'Figma' => [
        'https://www.figma.com/file/x5zBLbvoW1jsvyAOt4Gp9I',
        __DIR__ . '/../../fixtures/figma.json',
      ],
      'Canva' => [
        'https://www.canva.com/design/DAGOiJh0W8g/Zfu4PhoJydf84f4LSU9RBg/view',
        __DIR__ . '/../../fixtures/canva.json',
      ],
    ];
  }

  /**
   * Tests creating remote document media from various providers.
   *
   * @param string $url
   *   The URL of the remote media to add. In a real site, this would be used to
   *   create the media in the UI.
   * @param string $fixture_file
   *   The path of the local file containing the oEmbed resource data.
   *
   * @dataProvider providerRemoteDocumentMedia
   */
  public function testRemoteDocumentMedia(string $url, string $fixture_file): void {
    $media = $this->createRemoteDocumentMedia($url, $fixture_file);

    // The resource title should be reused as the alt text of the thumbnail.
    $source = $media->getSource();
    $plugin_definition = $source->getPluginDefinition();
    $this->assertSame($media->label(), $source->getMetadata($media, $plugin_definition['thumbnail_alt_metadata_attribute']));
  }

  /**
   * Creates a remote document media item.
   *
   * @param string $url
   *   The URL of the remote media to add. In a real site, this would be used to
   *   create the media in the UI.
   * @param string $fixture_file
   *   The path of the local file containing the oEmbed resource data.
   *
   * @return \Drupal\media\Entity\Media
   *   The created media item.
   */
  private function createRemoteDocumentMedia(string $url, string $fixture_file): Media {
    // Ensure the resource data can be fetched by the test middleware.
    $resource_url = $this->container->get('media.oembed.url_resolver')
      ->getResourceUrl($url);

    /** @var \Drupal\Core\State\StateInterface $state */
    $state = $this->container->get('state');
    $paths = [
      $resource_url => $fixture_file,
    ];
    $state->set(ResourceMiddleware::class, $paths);

    // Ensure the the thumbnail can be fetched by our test middleware.
    $thumbnail_url = $this->container->get('media.oembed.resource_fetcher')
      ->fetchResource($resource_url)
      ->getThumbnailUrl();
    $this->assertNotEmpty($thumbnail_url);
    $thumbnail_url = $thumbnail_url->toString();
    $paths[$thumbnail_url] = $this->getDrupalRoot() . '/core/misc/druplicon.png';
    $state->set(ResourceMiddleware::class, $paths);

    // Ensure we can create a remote document media item from the given fixture.
    $media = Media::create([
      'bundle' => 'remote_document',
      'field_media_oembed_document' => $url,
    ]);
    $media->save();

    return $media;
  }

}

