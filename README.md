# Media Remote Document

A simple module that extends oEmbed support added to Drupal Core's Media module
by implementing hook_media_source_info_alter() for the following providers:
- ShareFile


## Requirements

Drupal Core Media (media) module.


## Installation

`composer require drupal/media_remote_document:^1.0`


## Configuration

This module does not have configuration.


## Features

- **Media type:** Creates a 'Remote document' Media type
- **Manage fields:** Creates an 'Document URL' (field_media_oembed_document) plain
  text field
- **Manage form display:** Uses the 'oEmbed URL' widget
- **Manage display:** Uses the 'oEmbed content' widget


## Usage:

To add Remote document content, simply go to `/media/add/remote_document`
and paste the document url from Figma or Canva.
