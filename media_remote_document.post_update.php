<?php

/**
 * @file
 * Contains post-update hooks for Media Remote Document.
 */

/**
 * Ensures the remote document media source has the correct metadata attributes.
 */
function media_remote_document_post_update_fix_thumbnail_metadata_attributes() {
  // Deliberately empty in order to force a container rebuild.
}
